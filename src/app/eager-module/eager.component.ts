import { Component, OnInit } from '@angular/core';
import {SharedService} from '../shared-service-module/shared.service';

@Component({
  selector: 'app-eager',
  template: '<p>Hello from eager component. My service has id {{getServiceId()}}</p>'
})
export class EagerComponent {

  constructor(private sharedService: SharedService) { }

  getServiceId(): number {
    return this.sharedService.getId();
  }

}
