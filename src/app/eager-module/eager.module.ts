import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EagerComponent } from './eager.component';
import {SharedServiceModule} from '../shared-service-module/shared-service.module';

@NgModule({
  imports: [
    CommonModule,
    SharedServiceModule
  ],
  declarations: [EagerComponent]
})
export class EagerModule { }
