import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EagerComponent} from './eager-module/eager.component';

const routes: Routes = [
  { path: '', redirectTo: 'eager', pathMatch: 'full' },
  { path: 'eager', component: EagerComponent },
  { path: 'lazy', loadChildren: 'app/lazy-module/lazy.module#LazyModule' }
];

export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(routes);
