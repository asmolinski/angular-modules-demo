import { Injectable } from '@angular/core';

@Injectable()
export class SharedService {

  private readonly id = Math.floor(Math.random()*100000)

  constructor() {
    console.log('Shared service is being created!');
  }

  getId() {
    return this.id;
  }
}
