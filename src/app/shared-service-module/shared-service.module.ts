import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedService} from './shared.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  //providers: [SharedService]
})
export class SharedServiceModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedServiceModule,
      providers: [SharedService]
    };
  }
}
