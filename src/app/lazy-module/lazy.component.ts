import { Component } from '@angular/core';
import {SharedService} from '../shared-service-module/shared.service';

@Component({
  selector: 'app-lazy',
  template: '<p>Hello from lazy component. My service has id {{getServiceId()}}</p>'
})
export class LazyComponent {

  constructor(private sharedService: SharedService) { }

  getServiceId(): number {
    return this.sharedService.getId();
  }

}
