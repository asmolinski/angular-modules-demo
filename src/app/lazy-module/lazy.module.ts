import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LazyComponent} from './lazy.component';
import {SharedServiceModule} from '../shared-service-module/shared-service.module';

const routes: Routes = [
  { path: '', component: LazyComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedServiceModule
  ],
  declarations: [LazyComponent]
})
export class LazyModule { }
