import { ModulesDemoPage } from './app.po';

describe('modules-demo App', () => {
  let page: ModulesDemoPage;

  beforeEach(() => {
    page = new ModulesDemoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
